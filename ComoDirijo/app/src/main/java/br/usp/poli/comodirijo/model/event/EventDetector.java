package br.usp.poli.comodirijo.model.event;

import android.util.Log;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import br.usp.poli.comodirijo.MyApplication;
import br.usp.poli.comodirijo.model.route.list.RouteItem;
import br.usp.poli.comodirijo.network.web.RequestTask;
import br.usp.poli.comodirijo.util.Settings;

/**
 * Created by danilojun on 12/11/15.
 */
public class EventDetector {

    private static final String TAG = "EventDetector";

    // Thresholds
    private final double ACC_X = 0.6;
    private final double ACC_Y = 0.7;
    private final int MAX_RPM = 3500;
//    private final int MIN_RPM = 500;

    private boolean suddenAccelerationAlreadyDetected = false;
    private boolean suddenBrakeAlreadyDetected = false;
    private boolean suddenTurnAlreadyDetected = false;
    private boolean overSpeedAlreadyDetected = false;
    private boolean overRPMAlreadyDetected = false;
//    private boolean lowRPMAlreadyDetected = false;

    public ArrayList<EventType> detectEvents(List<JSONObject> JSONList, float maxSpeed) {

        ArrayList<EventType> faults = new ArrayList<>();

        try {
            // only get the first object (correction of bug)
            for (int i = 0; i < 1; i++) {
                JSONObject mJSONObject = JSONList.get(i);

                // TODO : improve this logic: create JSON deserializer for the raspberry response

                if (mJSONObject.has("velocidade")) {
//                    Log.d(TAG, "Max speed is: " + maxSpeed);
                    if (!overSpeedAlreadyDetected && Float.parseFloat(mJSONObject.getString("velocidade")) > maxSpeed) {
                        faults.add(EventType.OVER_MAX_SPEED);
                        overSpeedAlreadyDetected = true;

                        Log.d(TAG, EventType.OVER_MAX_SPEED.toString() + " : " + mJSONObject.getString("velocidade"));

                    } else if (Float.parseFloat(mJSONObject.getString("velocidade")) <= maxSpeed) {
                        overSpeedAlreadyDetected = false;
                    }
                }

                if (mJSONObject.has("aceleracaoX")) {
                    double accX = Double.parseDouble(mJSONObject.getString("aceleracaoX"));

                    if (!suddenAccelerationAlreadyDetected && accX > ACC_X && accX > 0) {
                        faults.add(EventType.SUDDEN_ACCELERATION);
                        suddenBrakeAlreadyDetected = true;
                    } else if (accX <= ACC_X && accX > 0) {
                        suddenAccelerationAlreadyDetected = false;
                    }

                    if (!suddenBrakeAlreadyDetected && Math.abs(accX) > ACC_X && accX < 0) {
                        faults.add(EventType.SUDDEN_BRAKE);
                        suddenBrakeAlreadyDetected = true;
                    } else if (Math.abs(accX) <= ACC_X && accX < 0) {
                        suddenBrakeAlreadyDetected = false;
                    }
                }

                if (mJSONObject.has("aceleracaoY")) {
                    if (!suddenTurnAlreadyDetected && Double.parseDouble(mJSONObject.getString("aceleracaoY")) > ACC_Y) {
                        faults.add(EventType.SUDDEN_TURN);
                        suddenTurnAlreadyDetected = true;
                    } else if (Double.parseDouble(mJSONObject.getString("aceleracaoY")) <= ACC_Y) {
                        suddenTurnAlreadyDetected = false;
                    }
                }

                if (mJSONObject.has("rpm")) {
                    if (!overRPMAlreadyDetected && Integer.parseInt(mJSONObject.getString("rpm")) > MAX_RPM) {
                        faults.add(EventType.HIGH_RPM);
                        overRPMAlreadyDetected = true;

                        Log.d(TAG, EventType.HIGH_RPM.toString() + " : " + mJSONObject.getString("rpm"));

                    } else if (Integer.parseInt(mJSONObject.getString("rpm")) <= MAX_RPM) {
                        overRPMAlreadyDetected = false;
                    }

//                    if (!lowRPMAlreadyDetected && Integer.parseInt(mJSONObject.getString("rpm")) < MIN_RPM && Integer.parseInt(mJSONObject.getString("rpm")) > 0) {
//                        faults.add(EventType.LOW_RPM);
//                        lowRPMAlreadyDetected = true;
//
//                        Log.d(TAG, EventType.LOW_RPM.toString() + " : " + mJSONObject.getString("rpm"));
//                    } else if (Integer.parseInt(mJSONObject.getString("rpm")) >= MIN_RPM) {
//                        lowRPMAlreadyDetected = false;
//                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            faults.add(EventType.UNKNOWN);
        }

        return faults;
    }

}
