package br.usp.poli.comodirijo.model.route.list;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.Date;

import br.usp.poli.comodirijo.MyApplication;

/**
 * Created by danilojun on 05/11/15.
 */
public class RouteItemDeserializer implements JsonDeserializer<RouteItem> {
    @Override
    public RouteItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        String id = json.getAsJsonObject().get("_id").getAsString();
        String name = json.getAsJsonObject().get("name").getAsString();

        JsonObject date = json.getAsJsonObject().get("date").getAsJsonObject();

        Date dateStart;
        try {
            dateStart = MyApplication.dateFormat.parse(date.get("start").getAsString());
        } catch (ParseException e) {
            e.printStackTrace();
            // set dummy date in case of exception
            dateStart = new Date();
        }

        return new RouteItem(id, name, dateStart);
    }
}
