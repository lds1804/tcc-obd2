package br.usp.poli.comodirijo.model.route;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import br.usp.poli.comodirijo.MyApplication;

/**
 * Created by danilojun on 05/11/15.
 */
public class RouteDeserializer implements JsonDeserializer<Route> {
    @Override
    public Route deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        String name = json.getAsJsonObject().get("name").getAsString();
        String driver = json.getAsJsonObject().get("driver").getAsString();
        int score = json.getAsJsonObject().get("score").getAsInt();

        String locationStart = json.getAsJsonObject().get("location").getAsJsonObject().get("start").getAsString();
        String locationEnd = json.getAsJsonObject().get("location").getAsJsonObject().get("end").getAsString();

        JsonObject date = json.getAsJsonObject().get("date").getAsJsonObject();

        Date dateStart;
        Date dateEnd;
        try {
            dateStart = MyApplication.dateFormat.parse(date.get("start").getAsString());
            dateEnd = MyApplication.dateFormat.parse(date.get("end").getAsString());
        } catch (ParseException e) {
            e.printStackTrace();
            // set some dummy dates in case of exception
            dateStart = new Date();
            dateEnd = new Date();
        }
        JsonObject numberOfEvents = json.getAsJsonObject().get("numberOfEvents").getAsJsonObject();
        int overMaxSpeed = numberOfEvents.get("overMaxSpeed").getAsInt();
        int suddenBrake = numberOfEvents.get("suddenBrake").getAsInt();
        int suddenAcceleration = numberOfEvents.get("suddenAcceleration").getAsInt();
        int suddenTurn = numberOfEvents.get("suddenTurn").getAsInt();
        int highRpm = numberOfEvents.get("highRpm").getAsInt();
        int lowRpm = numberOfEvents.get("lowRpm").getAsInt();

        JsonObject metrics = json.getAsJsonObject().get("metrics").getAsJsonObject();
        float maxSpeed = metrics.get("maxSpeed").getAsFloat();
        float maxAcceleration = metrics.get("maxAcceleration").getAsFloat();

        return new Route(name, driver, score, locationStart, locationEnd, dateStart, dateEnd, overMaxSpeed, suddenBrake,
                suddenAcceleration, suddenTurn, highRpm, lowRpm, maxSpeed, maxAcceleration);
    }
}
