package br.usp.poli.comodirijo.network.web;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import br.usp.poli.comodirijo.util.Settings;

/**
 * Created by danilojun on 21/10/15.
 *
 * Class to perform an asynchronous request to web server
 */
public class RequestTask extends AsyncTask<String, Integer, String> {
    // Log tag
    private static final String TAG = "RequestTask";

    private Context context;
    private URL url;

    public RequestTask(Context context, String route) throws MalformedURLException {
        this.context = context;
        this.url = new URL(Settings.SERVER_ADDRESS + "/" + route);
    }

    @Override
    protected String doInBackground(String... params) {

        String result;

        // Gets the URL from the UI's text field.
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        // params comes from the execute() call: params[0] is the url.
        if (networkInfo != null && networkInfo.isConnected()) try {
            result = sendPost(params[0]);
        } catch (IOException e) {
            e.printStackTrace();
            result = "Unable to retrieve web page. URL may be invalid.";
        }
        else {
            result = "No network connection available";
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG, result);
        Toast toast = Toast.makeText(context, result, Toast.LENGTH_SHORT);
        toast.show();
    }

    // Sends a message
    private String sendPost(String data) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");

            conn.setDoInput(true);
            conn.setDoOutput(true);

            Log.d(TAG, "url is: " + url.toString());

            os = conn.getOutputStream();
            os.write(data.getBytes(Charset.forName("UTF-8")));

            // Starts the query
            conn.connect();
            int responseCode = conn.getResponseCode();

            Log.d(TAG, "The responseCode is: " + responseCode);

            is = conn.getInputStream();
            // Convert the InputStream into a string
            return getStringFromInputStream(is);
        } finally {
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
        }
    }

    // convert InputStream to String
    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
}
