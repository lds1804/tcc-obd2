package br.usp.poli.comodirijo.model.event.list;

/**
 * Created by danilojun on 08/11/15.
 */
public class EventItem {

    public String label;
    public int number;

    public EventItem(String label, int number) {
        this.label = label;
        this.number = number;
    }
}
