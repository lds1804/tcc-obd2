package br.usp.poli.comodirijo.model.route.chart;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.Date;

import br.usp.poli.comodirijo.MyApplication;

/**
 * Created by danilojun on 09/12/15.
 */
public class RouteChartPointDeserializer implements JsonDeserializer<RouteChartPoint> {
    @Override
    public RouteChartPoint deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        float score = json.getAsJsonObject().get("score").getAsFloat();

        Date date;
        try {
            date = MyApplication.dateFormat.parse(json.getAsJsonObject().get("date").getAsString());
        } catch (ParseException e) {
            e.printStackTrace();
            // set some dummy dates in case of exception
            date = new Date();
        }

        return new RouteChartPoint(score, date);
    }
}
