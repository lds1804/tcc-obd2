package br.usp.poli.comodirijo.model.route;

import java.util.Date;

import br.usp.poli.comodirijo.model.event.EventType;

/**
 * Created by danilojun on 27/10/15.
 */
public class Route {
    public String name;
    public String driver;

    public int score;

    public String locationStart;
    public String locationEnd;

    public Date dateStart;
    public Date dateEnd;

    public int numberOfEventsOverMaxSpeed;
    public int numberOfEventsSuddenBrake;
    public int numberOfEventsSuddenAcceleration;
    public int numberOfEventsSuddenTurn;
    public int numberOfHighRpm;
    public int numberOfLowRpm;

    public float maxSpeed;
    public float maxAcceleration;

    public Route(String driver) {
        this.driver = driver;

        this.dateStart = new Date();

        // set initial dummy strings
        this.locationStart = "";
        this.locationEnd = "";

        this.numberOfEventsOverMaxSpeed = 0;
        this.numberOfEventsSuddenBrake = 0;
        this.numberOfEventsSuddenAcceleration = 0;
        this.numberOfEventsSuddenTurn = 0;
        this.numberOfHighRpm = 0;
        this.numberOfLowRpm = 0;

        this.maxSpeed = 0;
        this.maxAcceleration = 0;
    }

    public Route(String name, String driver, int score, String locationStart, String locationEnd, Date dateStart, Date dateEnd,
                 int numberOfEventsOverMaxSpeed, int numberOfEventsSuddenBrake,
                 int numberOfEventsSuddenAcceleration, int numberOfEventsSuddenTurn,
                 int numberOfHighRpm, int numberOfLowRpm,
                 float maxSpeed, float maxAcceleration) {
        this.name = name;
        this.driver = driver;
        this.score = score;
        this.locationStart = locationStart;
        this.locationEnd = locationEnd;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.numberOfEventsOverMaxSpeed = numberOfEventsOverMaxSpeed;
        this.numberOfEventsSuddenBrake = numberOfEventsSuddenBrake;
        this.numberOfEventsSuddenAcceleration = numberOfEventsSuddenAcceleration;
        this.numberOfEventsSuddenTurn = numberOfEventsSuddenTurn;
        this.numberOfHighRpm = numberOfHighRpm;
        this.numberOfLowRpm = numberOfLowRpm;
        this.maxSpeed = maxSpeed;
        this.maxAcceleration = maxAcceleration;
    }

    public void addEvent(EventType event) {
        switch (event) {
            case OVER_MAX_SPEED:
                numberOfEventsOverMaxSpeed++;
                break;
            case SUDDEN_BRAKE:
                numberOfEventsSuddenBrake++;
                break;
            case SUDDEN_ACCELERATION:
                numberOfEventsSuddenAcceleration++;
                break;
            case SUDDEN_TURN:
                numberOfEventsSuddenTurn++;
                break;
            case HIGH_RPM:
                numberOfHighRpm++;
            case LOW_RPM:
                numberOfLowRpm++;
                break;
        }
    }
}
