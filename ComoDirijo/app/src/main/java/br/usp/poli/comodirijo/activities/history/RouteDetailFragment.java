package br.usp.poli.comodirijo.activities.history;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.net.MalformedURLException;
import java.util.ArrayList;

import br.usp.poli.comodirijo.MyApplication;
import br.usp.poli.comodirijo.R;
import br.usp.poli.comodirijo.model.event.list.EventItem;
import br.usp.poli.comodirijo.model.route.Route;
import br.usp.poli.comodirijo.network.web.RequestTask;

/**
 * A fragment representing a single RouteDetailActivity detail screen.
 * This fragment is either contained in a {@link RouteListActivity}
 * in two-pane mode (on tablets) or a {@link RouteDetailActivity}
 * on handsets.
 */
public class RouteDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    private Route route;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RouteDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_route_detail, container, false);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            try {
                JsonObject json = new JsonObject();
                json.addProperty("id", getArguments().getString(ARG_ITEM_ID));
                RequestTask getRouteInfoTask = new RequestTask(getActivity(), "route") {
                    @Override
                    protected void onPostExecute(String result) {
                        route = MyApplication.gson.fromJson(result, Route.class);
                        updateFragment();
                    }
                };
                getRouteInfoTask.execute(json.toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        updateFragment();
        super.onActivityCreated(savedInstanceState);
    }

    /**
     * Auxiliar methods
     */

    private void updateFragment() {
        if (route != null) {
            populateAndShowRoute();

            ProgressBar progress = (ProgressBar) getActivity().findViewById(R.id.progress_bar_route_detail);
            progress.setVisibility(View.GONE);

            ViewGroup eventDetails = (ViewGroup) getActivity().findViewById(R.id.route_details);
            eventDetails.setVisibility(View.VISIBLE);
        } else {
            ViewGroup eventDetails = (ViewGroup) getActivity().findViewById(R.id.route_details);
            eventDetails.setVisibility(View.GONE);

            ProgressBar progress = (ProgressBar) getActivity().findViewById(R.id.progress_bar_route_detail);
            progress.setVisibility(View.VISIBLE);
            progress.setIndeterminate(true);
        }
    }

    private void populateAndShowRoute() {
        getActivity().setTitle(route.name);

        // set score
        TextView score = (TextView) getActivity().findViewById(R.id.route_details_score);
        score.setText(String.valueOf(route.score));

        // set locations
        TextView locationStart = (TextView) getActivity().findViewById(R.id.location_content_from);
        locationStart.setText(route.locationStart);
        TextView locationEnd= (TextView) getActivity().findViewById(R.id.location_content_to);
        locationEnd.setText(route.locationEnd);

        // set dates
        TextView dateStart = (TextView) getActivity().findViewById(R.id.date_start);
        dateStart.setText(route.dateStart.toString());
        TextView dateEnd = (TextView) getActivity().findViewById(R.id.date_end);
        dateEnd.setText(route.dateEnd.toString());

        // set events

        ArrayList<EventItem> events = new ArrayList<>();
        // TODO : more generic code? maybe it is a good idea to çreate a class Event and use it in Route
        if (route.numberOfEventsOverMaxSpeed > 0) {
            events.add(new EventItem("Over max speed", route.numberOfEventsOverMaxSpeed));
        }
        if (route.numberOfEventsSuddenBrake > 0) {
            events.add(new EventItem("Sudden brake", route.numberOfEventsSuddenBrake));
        }
        if (route.numberOfEventsSuddenAcceleration > 0) {
            events.add(new EventItem("Sudden acceleration", route.numberOfEventsSuddenAcceleration));
        }
        if (route.numberOfEventsSuddenTurn > 0) {
            events.add(new EventItem("Sudden turn", route.numberOfEventsSuddenTurn));
        }
        if (route.numberOfHighRpm > 0) {
            events.add(new EventItem("High RPM", route.numberOfHighRpm));
        }
//        if (route.numberOfLowRpm > 0) {
//            events.add(new EventItem("Low RPM", route.numberOfLowRpm));
//        }

        ListView eventsList = (ListView) getActivity().findViewById(R.id.route_details_events);
        eventsList.setAdapter(new EventListArrayAdapter(getActivity(), events));
    }

    private class EventListArrayAdapter extends ArrayAdapter<EventItem> {

        private final Context context;
        private final ArrayList<EventItem> values;

        public EventListArrayAdapter(Context context, ArrayList<EventItem> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_item_event, parent, false);
            }

            TextView labelTextView = (TextView) convertView.findViewById(R.id.event_label);
            labelTextView.setText(values.get(position).label);
            TextView numberTextView = (TextView) convertView.findViewById(R.id.event_number);
            numberTextView.setText(String.valueOf(values.get(position).number));

            return convertView;
        }
    }
}
