package br.usp.poli.comodirijo.util;

/**
 * Created by danilojun on 22/10/15.
 */
public class Settings {

    public static final String PREFS_NAME = "ComoDirijoPreferences";

    // TODO : set correct mac address of raspberry pi bluetooth module
    public static final String BLUETOOTH_MAC_ADDRESS = "123";
    // TODO : set correct UUID of raspberry pi bluetooth app
    public static final String MY_UUID = "94f39d29-7d6d-437d-973b-fba39e49d4ee";
    // TODO : set correct server address
//    public static final String SERVER_ADDRESS = "http://192.168.0.123:3000";
    public static final String SERVER_ADDRESS = "https://comodirijo.herokuapp.com";
}
