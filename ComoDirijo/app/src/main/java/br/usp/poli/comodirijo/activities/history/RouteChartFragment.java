package br.usp.poli.comodirijo.activities.history;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.gson.JsonObject;

import java.net.MalformedURLException;
import java.util.ArrayList;

import br.usp.poli.comodirijo.MyApplication;
import br.usp.poli.comodirijo.R;
import br.usp.poli.comodirijo.model.route.chart.RouteChartPoint;
import br.usp.poli.comodirijo.network.web.RequestTask;
import br.usp.poli.comodirijo.util.Settings;

public class RouteChartFragment extends Fragment {

    private static final String TAG = "RouteChartFragment";

    private LineChart chart;
    private RouteChartPoint[] routeChartPoints;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_route_chart, container, false);

        try {
            JsonObject json = new JsonObject();
            json.addProperty("userId", MyApplication.USER_ID);
            RequestTask populateListTask = new RequestTask(getActivity(), "scores") {
                @Override
                protected void onPostExecute(String result) {
                    // get chart data
                    routeChartPoints = MyApplication.gson.fromJson(result, RouteChartPoint[].class);
                    updateChart();
                }
            };
            populateListTask.execute(json.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        initChart();
        updateChart();
        super.onActivityCreated(savedInstanceState);
    }

    private void initChart() {
        chart = (LineChart) getActivity().findViewById(R.id.routes_chart);
        // format chart
        chart.setDescription("scores over time");
        chart.setAutoScaleMinMaxEnabled(true);
        chart.setBackgroundColor(getResources().getColor(R.color.background_material_light));
    }

    private void updateChart() {
        if (routeChartPoints != null) {
            // build chart
            // y axis values
            ArrayList<Entry> routeEntries = new ArrayList<>();

            for (int i = 0; i < routeChartPoints.length; i++) {
                Entry entry = new Entry(routeChartPoints[i].score, i);
                routeEntries.add(entry);
            }

            LineDataSet dataSet = new LineDataSet(routeEntries, "Score");
            dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

            // change colors
            int color = Color.rgb(0, 188, 212);
            dataSet.setCircleColor(color);
            dataSet.setHighLightColor(color);
            dataSet.setFillColor(color);

            ArrayList<LineDataSet> dataSets = new ArrayList<>();
            dataSet.setLineWidth(2f);
            dataSet.setDrawFilled(true);
            dataSets.add(dataSet);

            // x axis values
            ArrayList<String> xValues = new ArrayList<>();
            for (int i = 0; i < routeChartPoints.length; i++) {
                xValues.add(i, MyApplication.dateFormatToShow.format(routeChartPoints[i].date));
            }

            LineData data = new LineData(xValues, dataSets);
            chart.setData(data);
            chart.invalidate(); // refresh
        }
    }
}
