package br.usp.poli.comodirijo.activities.route;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.usp.poli.comodirijo.MyApplication;
import br.usp.poli.comodirijo.R;
import br.usp.poli.comodirijo.activities.history.RouteListActivity;
import br.usp.poli.comodirijo.model.event.EventDetector;
import br.usp.poli.comodirijo.model.event.EventType;
import br.usp.poli.comodirijo.model.route.Route;
import br.usp.poli.comodirijo.network.bluetooth.BluetoothService;
import br.usp.poli.comodirijo.network.web.RequestTask;

public class RouteActivity extends FragmentActivity implements OnMapReadyCallback,
        ConnectionCallbacks, OnConnectionFailedListener, LocationListener,
        RouteNameDialogFragment.RouteNameDialogListener {

    private static final String TAG = "RouteActivity";

//    private static final int REQUEST_ENABLE_BT = 1;
//    private static final String ACELERACAO = "Aceleracao Brusca";

    // maps and location
    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private LocationRequest locationRequest;
    private Polyline routePolyline;

    private static final double LOC_CHANGE_DIST_KM = 0.300;
    private boolean locationChanged = false;
    private boolean firstLocation = true;

    // bluetooth connection and communication
    private BluetoothService mBluetoothService;
    private BluetoothAdapter mBluetoothAdapter;

    // events detection
    private Route route;
    private List<Marker> eventsInMap;
    private EventDetector eventDetector;

    // speed limit detection
    private float speedLimit = 100; // initial speed limit

    /**
     * Handler to detect events
     */
    static class EventHandler extends Handler {
        private final WeakReference<RouteActivity> routeActivityWeakReference;

        EventHandler (RouteActivity routeActivity) {
            routeActivityWeakReference = new WeakReference<RouteActivity>(routeActivity);
        }

        @Override
        public void handleMessage(Message msg) {

            String textoRecebido = msg.getData().getString("dadosVelocidade");
            Log.d("Toast", textoRecebido);

            final List<JSONObject> mJsonList = routeActivityWeakReference.get().trataJson(textoRecebido);

            // if the address changed (or it is the first location), get current speed limit
            if (routeActivityWeakReference.get().firstLocation || routeActivityWeakReference.get().locationChanged) {
                routeActivityWeakReference.get().firstLocation = false;
                // get speed limit
                try {
                    JsonObject json = new JsonObject();
                    json.addProperty("latitude", routeActivityWeakReference.get().lastLocation.getLatitude());
                    json.addProperty("longitude", routeActivityWeakReference.get().lastLocation.getLongitude());
                    RequestTask populateListTask = new RequestTask(routeActivityWeakReference.get(), "maxSpeed") {
                        @Override
                        protected void onPostExecute(String result) {
                            try {
                                routeActivityWeakReference.get().speedLimit = Float.parseFloat(result);
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            routeActivityWeakReference.get().detectEvents(mJsonList);
                        }
                    };
                    populateListTask.execute(json.toString());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                routeActivityWeakReference.get().detectEvents(mJsonList);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        // Obtain the SupportMapFragment and get notified when the googleMap is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initGoogleApiClient();

        // init route
        route = new Route(MyApplication.USER_ID);
        eventsInMap = new ArrayList<>();
        eventDetector = new EventDetector();

        // setup Raspberry communication
        mBluetoothService = new BluetoothService(new EventHandler(this));
        mBluetoothService.start();
        mBluetoothService.connect();

        //mBluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        //checa se o adaptador esta habilitado se nao estiver habilita-o
//        if (!mBluetoothAdapter.isEnabled())
//        {
//            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            try {
//                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
//            } catch (Exception e )
//            {
//                Log.e("Bluetooth Service","Error trying to enable bluetooth"+ e);
//
//
//            }
//
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMyLocationEnabled(true);

        this.routePolyline = googleMap.addPolyline(new PolylineOptions()
                .width(15)
                .color(Color.rgb(0, 186,210))
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        if (googleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    /* Event handling methods */

    // TODO : today, the toasts may be launched overlapped... change the way to show the events!
    private void showEvent(String informacaoToast) {
        Log.d(TAG, "show event: " + informacaoToast);
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(this, informacaoToast, duration);
        toast.show();
    }

    private List<JSONObject> trataJson(String textoRecebido) {
        int indexStart, indexStop;
        indexStart = -1;
        indexStop = -1;
        List<JSONObject> json;
        JSONObject mJsonObject;
        json = new ArrayList<>();
        for (int i = 0; i < textoRecebido.length(); i++) {
            if ((textoRecebido.charAt(i)) == '{')
                indexStart = i;
            if ((textoRecebido.charAt(i)) == '}')
                indexStop = i;
            if (indexStart != -1 && indexStop != -1) {
                try {
                    mJsonObject = new JSONObject(textoRecebido.substring(indexStart, indexStop + 1));
                    json.add(mJsonObject);
                    indexStart = -1;
                    indexStop = -1;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return json;

    }

    private void registerEvents(ArrayList<EventType> events) {
        for(EventType event : events) {
            route.addEvent(event);
            Log.d(TAG, "Event registered " + event.toString());
        }
    }

    private void showEvents(ArrayList<EventType> events) {

        String message = "";

        for (EventType event : events) {
            message += event.toString() + "\n";
        }

        if (!events.isEmpty()) {
            showEvent(message);
            if (lastLocation != null) {
                Marker marker = googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()))
                        .title("Events")
                        .snippet(message));

                eventsInMap.add(marker);
            }
        }
    }

    private void detectEvents(List<JSONObject> mJsonList) {
        ArrayList<EventType> events = eventDetector.detectEvents(mJsonList, speedLimit);
        registerEvents(events);
        showEvents(events);
    }

    /* Raspberry communication handling methods */
    private void stopRaspberryUpdates() {
        mBluetoothService.pause();
    }

    private void endBluetoothThreads() {
        mBluetoothService.stop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "Destroy");
        endBluetoothThreads();
        super.onDestroy();
    }

    /* Location handling methods */

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "googleAPI onConnected");

        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            updateUI();
            // set start location
            route.locationStart = getAddressFromLocation(lastLocation);
        }

        createLocationRequest();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection suspended: " + Integer.toString(i));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        // boolean to verify if location had significant change
        locationChanged = locationChanged(location);
        // update location
        lastLocation = location;
        // set start location if it is not set
        if (route.locationStart.equals("Unavailable")) {
            route.locationStart = getAddressFromLocation(location);
        }
        updateUI();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    protected synchronized void initGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    private void updateUI() {
        LatLng coordinates = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

         // center map to current location
//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), 8.0f));

        // update route trace
        List<LatLng> points = routePolyline.getPoints();
        points.add(coordinates);
        routePolyline.setPoints(points);
    }

    // get address from location
    public String getAddressFromLocation(Location location) {

        String address = "Unavailable";

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            address = addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return address;
    }

    // verify if location had significant change
    private boolean locationChanged(Location newLocation) {
        return getDistanceFromLatLonInKm(lastLocation, newLocation) > LOC_CHANGE_DIST_KM;
    }

    private double getDistanceFromLatLonInKm(Location loc1, Location loc2) {
        double R = 6371; // Radius of the earth in km

        double dLat = deg2rad(loc2.getLatitude() - loc2.getLatitude());  // deg2rad below
        double dLon = deg2rad(loc2.getLongitude() - loc2.getLongitude());

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos(deg2rad(loc1.getLatitude())) * Math.cos(deg2rad(loc2.getLatitude())) *
                                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c; // Distance in km
    }

    private double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }

    /* UI handling methods */

    public void stopRoute(View view) {
        Log.d(TAG, "End of route!");

        // stop getting events
        stopLocationUpdates();
        endBluetoothThreads();

        // ask Route name
        DialogFragment dialog = new RouteNameDialogFragment();
        dialog.show(getSupportFragmentManager(), "routeName");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // set route attributes
        EditText routeName = (EditText) dialog.getDialog().findViewById(R.id.input_route_name);
        route.name = routeName.getText().toString();
        route.dateEnd = new Date();
        route.locationEnd = getAddressFromLocation(lastLocation);
        // save and show history
        saveRouteAndGoToHistory();
    }

    private void saveRouteAndGoToHistory() {
        try {
            RequestTask getRouteInfoTask = new RequestTask(this, "routeCreateApp") {
                @Override
                protected void onPostExecute(String result) {
                    // go to history
                    Intent intent = new Intent(getApplicationContext(), RouteListActivity.class);
                    startActivity(intent);
                }
            };
            getRouteInfoTask.execute(MyApplication.gson.toJson(route));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}


