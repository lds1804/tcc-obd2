package br.usp.poli.comodirijo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by danilojun on 27/10/15.
 */
public class User {
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;

    @SerializedName("login")
    private String login;
    @SerializedName("password")
    private String password;

    public User(String firstName, String lastName, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.password = password;
    }
}
