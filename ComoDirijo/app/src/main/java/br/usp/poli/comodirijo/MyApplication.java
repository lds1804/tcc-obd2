package br.usp.poli.comodirijo;

import android.app.Application;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import br.usp.poli.comodirijo.model.route.Route;
import br.usp.poli.comodirijo.model.route.RouteDeserializer;
import br.usp.poli.comodirijo.model.route.RouteSerializer;
import br.usp.poli.comodirijo.model.route.chart.RouteChartPointDeserializer;
import br.usp.poli.comodirijo.model.route.chart.RouteChartPoint;
import br.usp.poli.comodirijo.model.route.list.RouteItem;
import br.usp.poli.comodirijo.model.route.list.RouteItemDeserializer;

/**
 * Created by danilojun on 05/11/15.
 */
public class MyApplication extends Application {

    public static String USER_ID = ""; // this will be set in the app initialization

    public static final Gson gson;

    public static final SimpleDateFormat dateFormat;

    public static final SimpleDateFormat dateFormatToShow;

    static {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Route.class, new RouteSerializer());
        builder.registerTypeAdapter(Route.class, new RouteDeserializer());
        builder.registerTypeAdapter(RouteItem.class, new RouteItemDeserializer());
        builder.registerTypeAdapter(RouteChartPoint.class, new RouteChartPointDeserializer());
        gson = builder.create();

        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(TimeZone.getDefault());

        dateFormatToShow = new SimpleDateFormat("yyyy/MM/dd");
        dateFormatToShow.setTimeZone(TimeZone.getDefault());
    }
}
