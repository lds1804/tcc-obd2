package br.usp.poli.comodirijo.model.route.list;

import java.util.Date;

/**
 * Created by danilojun on 05/11/15.
 */
public class RouteItem {

    public String id;
    public String name;
    public Date dateStart;

    public RouteItem(String id, String name, Date dateStart) {
        this.id = id;
        this.name = name;
        this.dateStart = dateStart;
    }
}
