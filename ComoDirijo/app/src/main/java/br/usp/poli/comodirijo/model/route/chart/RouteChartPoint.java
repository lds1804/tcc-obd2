package br.usp.poli.comodirijo.model.route.chart;

import java.util.Date;

/**
 * Created by danilojun on 09/12/15.
 */
public class RouteChartPoint {
    public float score;
    public Date date;

    public RouteChartPoint(float score, Date date) {
        this.score = score;
        this.date = date;
    }
}
