package br.usp.poli.comodirijo.model.route;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import br.usp.poli.comodirijo.MyApplication;

/**
 * Created by danilojun on 05/11/15.
 */
public class RouteSerializer implements JsonSerializer<Route> {
    @Override
    public JsonElement serialize(Route src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject route = new JsonObject();

        route.addProperty("name", src.name);
        route.addProperty("driver", src.driver);
        route.addProperty("score", src.score);

        JsonObject location = new JsonObject();
        location.addProperty("start", src.locationStart);
        location.addProperty("end", src.locationEnd);
        route.add("location", location);

        JsonObject date = new JsonObject();
        date.addProperty("start", MyApplication.dateFormat.format(src.dateStart));
        date.addProperty("end", MyApplication.dateFormat.format(src.dateEnd));
        route.add("date", date);

        JsonObject numberOfEvents = new JsonObject();
        numberOfEvents.addProperty("overMaxSpeed", src.numberOfEventsOverMaxSpeed);
        numberOfEvents.addProperty("suddenBrake", src.numberOfEventsSuddenBrake);
        numberOfEvents.addProperty("suddenAcceleration", src.numberOfEventsSuddenAcceleration);
        numberOfEvents.addProperty("suddenTurn", src.numberOfEventsSuddenTurn);
        numberOfEvents.addProperty("highRpm", src.numberOfHighRpm);
        numberOfEvents.addProperty("lowRpm", src.numberOfLowRpm);
        route.add("numberOfEvents", numberOfEvents);

        JsonObject metrics = new JsonObject();
        metrics.addProperty("maxSpeed", src.maxSpeed);
        metrics.addProperty("maxAcceleration", src.maxAcceleration);
        route.add("metrics", metrics);

        return route;
    }
}
