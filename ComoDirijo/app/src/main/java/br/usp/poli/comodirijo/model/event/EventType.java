package br.usp.poli.comodirijo.model.event;

/**
 * Created by danilojun on 12/11/15.
 */
public enum EventType {
    OVER_MAX_SPEED,
    SUDDEN_BRAKE,
    SUDDEN_ACCELERATION,
    SUDDEN_TURN,
    HIGH_RPM,
    LOW_RPM,
    UNKNOWN;

    @Override
    public String toString() {
        switch (this) {
            case OVER_MAX_SPEED:
                return "Over max speed";
            case SUDDEN_BRAKE:
                return "Sudden brake";
            case SUDDEN_ACCELERATION:
                return "Sudden acceleration";
            case SUDDEN_TURN:
                return "Sudden turn";
            case HIGH_RPM:
                return "High RPM";
            case LOW_RPM:
                return "Low RPM";
            case UNKNOWN:
                return "Unknown";
            default:
                return "Unknown";
        }
    }

    public String getMessage() {
        switch (this) {
            case OVER_MAX_SPEED:
                return "Speed is over the limits of this route.";
            case SUDDEN_BRAKE:
                return "Braking too strong.";
            case SUDDEN_ACCELERATION:
                return "Acceleration too strong.";
            case SUDDEN_TURN:
                return "Turning too fast.";
            case HIGH_RPM:
                return "RPM is too high.";
            case LOW_RPM:
                return "RPM is too low.";
            case UNKNOWN:
                return "Unknown event.";
            default:
                return "Unknown event.";
        }
    }
}
