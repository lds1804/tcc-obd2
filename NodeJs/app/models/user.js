var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var UserSchema = new Schema({
  name: {
    first: String,
    last: String
  },
  credentials: {
    login: { type: String, required: true, trim: true, index: { unique: true } },
    password: { type: String, required: true }
  }
});

UserSchema.virtual('name.full')
  .get(function(){
    return this.name.first + ' ' + this.name.last;
  });

mongoose.model('User', UserSchema);
