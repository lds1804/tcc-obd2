var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var RouteSchema = new Schema({
  name: String,
  driver: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
  location: {
    start: String,
    end: String
  },
  date: {
    start: { type: Date, required: true },
    end: { type: Date, required: true }
  },
  numberOfEvents: {
    overMaxSpeed: { type: Number, default: 0 },
    suddenBrake: { type: Number, default: 0 },
    suddenAcceleration: { type: Number, default: 0 },
    suddenTurn: { type: Number, default: 0 },
    highRpm: { type: Number, default: 0 },
    lowRpm: { type: Number, default: 0 }
  },
  metrics: {
    maxSpeed: { type: Number, min: 0 },
    maxAcceleration: { type: Number, min: 0 }
  }
});

RouteSchema.virtual('score')
  .get(function(){
    var maxScore = 100;
    var events = this.numberOfEvents;
    return maxScore - events.overMaxSpeed - events.suddenBrake - events.suddenAcceleration - events.suddenTurn - events.highRpm;
  });

mongoose.model('Route', RouteSchema);
