var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  User = mongoose.model('User');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/user', function (req, res, next) {
  User.find(function (err, users) {
    if (err) return next(err);
    res.render('user', {
      title: 'Users View',
      users: users
    });
  });
});

router.post('/userCreate', function (req, res, next) {
  var user = new User({
    name: {
      first: req.body.firstName,
      last: req.body.lastName
    },
    credentials: {
      login: req.body.login,
      password: req.body.password
    }
  });

  user.save(function (err) {
    if (err) {
      console.log(err);
      res.send('Error: creating user');
    } else {
      console.log('User created: ' + user.name.full);
      res.redirect('/user');
    }
  });
});

router.post('/userDelete', function (req, res, next) {
  User.findByIdAndRemove(req.body.id, function (err, user) {
    if (err) {
      console.log(err);
      res.send('Error: deleting user');
    } else {
      console.log('User deleted');
      res.redirect('/user');
    }
  });
});

router.post('/login', function (req, res) {
  console.log(req.body);
  var query = {
    credentials: {
      login: req.body.login,
      password: req.body.password
    }
  };
  User.findOne(query, function (err, user) {
    if (err) {
      console.log(err);
      res.send('ERROR');
    } else if (!user) {
      console.log('Authentication failed');
      res.send('ERROR');
    } else {
      res.send(user._id.toString());
    }
  });
});
