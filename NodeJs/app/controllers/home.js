var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Home page'
  });
});

router.get('/barrier', function (req, res, next) {
  console.log('Received message: ' + req.query.message);
  res.send('Received message: ' + req.query.message);
});

router.post('/postTest', function (req, res, next) {
  console.log('Received parameters: ' + JSON.stringify(req.body, null, 5));
  res.send('This is the POST Test!');
});
