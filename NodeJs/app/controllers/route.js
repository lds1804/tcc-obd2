var _ = require('underscore');
var request = require('request');

var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Route = mongoose.model('Route'),
  User = mongoose.model('User');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/route', function (req, res, next) {
  Route
  .find()
  .populate('driver')
  .exec(function (err, routes) {
    if (err) return next(err);
    console.log(routes);
    res.render('route', {
      title: 'Routes View',
      routes: routes
    });
  });
});

router.post('/routeCreate', function (req, res, next) {

  var userId = mongoose.Types.ObjectId(req.body.driver);

  User
    .find({_id: userId})
    .exec(function (err, user) {
      if (err) {
        console.log(err);
        res.send('Error: creating route');
      } else if (!user) {
        res.send('Error: user does not exists ' + req.body.driver);
      } else {
        var route = new Route({
          driver: userId,
          name: req.body.name,
          location: {
            start: req.body.startLocation,
            end: req.body.endLocation
          },
          date: {
            start: new Date(),
            end: new Date()
          },
          numberOfEvents: {
            overMaxSpeed: req.body.overMaxSpeed,
            suddenBrake: req.body.suddenBrake,
            suddenAcceleration: req.body.suddenAcceleration,
            suddenTurn: req.body.suddenTurn,
            highRpm: req.body.highRpm,
            lowRpm: req.body.lowRpm
          },
          metrics: {
            maxSpeed: req.body.maxSpeed,
            maxAcceleration: req.body.maxAcceleration
          }
        });

        route.save(function (err) {
          if (err) {
            console.log(err);
            res.send('Error: creating route');
          } else {
            console.log('Route created: ' + route.driver + ' (' + route.score + ')');
            res.redirect('/route');
          }
        });
      }
    });
});

router.post('/routeCreateApp', function (req, res, next) {

  var userId = mongoose.Types.ObjectId(req.body.driver);

  User
    .find({_id: userId})
    .exec(function (err, user) {
      if (err) {
        console.log(err);
        res.send('Error: creating route');
      } else if (!user) {
        res.send('Error: user does not exists ' + req.body.driver);
      } else {
        console.log(req.body);
        var route = new Route(req.body);
        route.save(function (err) {
          if (err) {
            console.log(err);
            res.send('Error: creating route');
          } else {
            var successMsg = 'Route created: ' + route.name + ' (' + route.score + ')';
            console.log(successMsg);
            res.send(200, successMsg);
          }
        });
      }
    });
});

router.post('/routeDelete', function (req, res, next) {
  Route.findByIdAndRemove(req.body.id, function (err, user) {
    if (err) return next(err);
    console.log('Route deleted');
    res.redirect('/route');
  });
});

router.post('/userRoutes', function (req, res, next) {
  console.log(req.body);
  Route
    .find({ driver: req.body.userId })
    .select({ _id: 1, name: 1, date: 1 })
    .exec(function (err, routes) {
      if (err) return next(err);
      res.send(JSON.stringify(routes));
    });
});

router.post('/route', function (req, res, next) {
  console.log(req.body);
  Route
    .findById(req.body.id)
    .exec(function (err, route) {
      if (err) return next(err);
      var response = route.toObject();
      response.score = route.score;
      console.log(response);
      res.send(JSON.stringify(response));
    });
});

router.post('/scores', function(req, res) {
  console.log(req.body);
  Route
    .find({ driver : req.body.userId })
    .exec(function (err, routes) {
      if (err) return next(err);

      var response = [];
      _.each(routes, function(route){
        response.push({
          score: route.score,
          date: route.date.start
        });
      });

      response.sort(function (a,b) {
        return a.date - b.date;
      });

      res.send(JSON.stringify(response));
    });

});


router.post('/maxSpeed', function(req, res){
  console.log(req.body);

  var around = '300';

  var overPassAPIUrl = 'http://overpass-api.de/api/interpreter?data=[out:json];way(around:AROUND,LATITUDE,LONGITUDE)[%22maxspeed%22];out;';
  overPassAPIUrl = overPassAPIUrl.replace('AROUND', around);
  overPassAPIUrl = overPassAPIUrl.replace('LATITUDE', req.body.latitude);
  overPassAPIUrl = overPassAPIUrl.replace('LONGITUDE', req.body.longitude);

  var options = {
    url: overPassAPIUrl,
    method: 'GET',
    json:true
  };
  request(options, function(error, response, body){

    if(error) {
      console.log(error);
      res.send("error when getting max speed");
    } else {
      var maxSpeed = 0;

      var elements = body.elements;
      _.each(elements, function(element){
        if (element.tags && element.tags.maxspeed) {
          // always get the higher speed
          maxSpeed = maxSpeed < element.tags.maxspeed ? element.tags.maxspeed : maxSpeed;
        }
      });

      console.log("Max speed is: " + maxSpeed);

      maxSpeed = maxSpeed || 100; // arbitrary value for maxSpeed when it is not found
      res.send(maxSpeed);
    }
  });

});
