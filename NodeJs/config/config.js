var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var mongoUser = process.env.MONGO_USER;
var mongoPass = process.env.MONGO_PASS;
var port = process.env.PORT;

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'webserver'
    },
    port: 3000,
    db: 'mongodb://localhost:27017'
  },

  test: {
    root: rootPath,
    app: {
      name: 'webserver'
    },
    port: 3000,
    db: 'mongodb://localhost/webserver-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'webserver'
    },
    port: port,
    db: 'mongodb://' + mongoUser + ':' + mongoPass + '@ds027415.mongolab.com:27415/comodirijo'
  }
};

module.exports = config[env];
