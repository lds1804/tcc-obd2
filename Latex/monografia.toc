\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Lista de tabelas}{8}{section*.7}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{13}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{13}{section.1.1}
\contentsline {section}{\numberline {1.2}Motiva\IeC {\c c}\IeC {\~a}o}{13}{section.1.2}
\contentsline {section}{\numberline {1.3}Organiza\IeC {\c c}\IeC {\~a}o do Documento}{14}{section.1.3}
\contentsline {section}{\numberline {1.4}Reposit\IeC {\'o}rio online}{14}{section.1.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Abordagem do Projeto e Ideias}}{15}{chapter.2}
\contentsline {section}{\numberline {2.1}Ideias iniciais e alternativas do projeto}{15}{section.2.1}
\contentsline {section}{\numberline {2.2}Ideia escolhida: abordagem com smartphone e hardware embarcado}{18}{section.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Revis\IeC {\~a}o Bibliogr\IeC {\'a}fica}}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}The 100 Car Naturalistic Driving Study}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}How's My Driving}{21}{section.3.2}
\contentsline {section}{\numberline {3.3}Driver Evaluation System Using Mobile Phone and OBD-II System}{22}{section.3.3}
\contentsline {section}{\numberline {3.4}Cobli}{25}{section.3.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Especifica\IeC {\c c}\IeC {\~a}o}}{28}{chapter.4}
\contentsline {section}{\numberline {4.1}Vis\IeC {\~a}o Empresarial}{28}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Monetiza\IeC {\c c}\IeC {\~a}o do projeto}{28}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}Vis\IeC {\~a}o da Informa\IeC {\c c}\IeC {\~a}o}{29}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Elementos de Informa\IeC {\c c}\IeC {\~a}o do Sistema}{29}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Manipula\IeC {\c c}\IeC {\~a}o das Informa\IeC {\c c}\IeC {\~o}es}{29}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Fluxo das Informa\IeC {\c c}\IeC {\~o}es}{30}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Vis\IeC {\~a}o Computacional}{33}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Requisitos}{33}{subsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.1.1}Requisitos Funcionais}{33}{subsubsection.4.3.1.1}
\contentsline {subsubsection}{\numberline {4.3.1.2}Requisitos N\IeC {\~a}o Funcionais}{34}{subsubsection.4.3.1.2}
\contentsline {subsection}{\numberline {4.3.2}Casos de Uso}{35}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Fluxos do sistema}{36}{subsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.3.1}Software de Captura de Dados do Raspberry}{36}{subsubsection.4.3.3.1}
\contentsline {section}{\numberline {4.4}Vis\IeC {\~a}o da Engenharia/Infraestrutura}{40}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Arquitetura do sistema}{40}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}Vis\IeC {\~a}o Tecnol\IeC {\'o}gica}{42}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Protocolo OBD-II}{42}{subsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.1.1}Origem do Protocolo}{43}{subsubsection.4.5.1.1}
\contentsline {subsubsection}{\numberline {4.5.1.2}Ado\IeC {\c c}\IeC {\~a}o e Varia\IeC {\c c}\IeC {\~o}es no Protocolo}{43}{subsubsection.4.5.1.2}
\contentsline {subsection}{\numberline {4.5.2}ELM327}{43}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Raspberry pi}{44}{subsection.4.5.3}
\contentsline {subsubsection}{\numberline {4.5.3.1}Hardware e Perif\IeC {\'e}ricos}{45}{subsubsection.4.5.3.1}
\contentsline {subsection}{\numberline {4.5.4}Python}{46}{subsection.4.5.4}
\contentsline {subsubsection}{\numberline {4.5.4.1}Hist\IeC {\'o}ria}{47}{subsubsection.4.5.4.1}
\contentsline {subsubsection}{\numberline {4.5.4.2}Caracter\IeC {\'\i }sticas da Linguagem}{47}{subsubsection.4.5.4.2}
\contentsline {subsection}{\numberline {4.5.5}Bluetooth}{48}{subsection.4.5.5}
\contentsline {subsubsection}{\numberline {4.5.5.1}Hist\IeC {\'o}ria}{48}{subsubsection.4.5.5.1}
\contentsline {subsubsection}{\numberline {4.5.5.2}Protocolo RFCOMM}{49}{subsubsection.4.5.5.2}
\contentsline {subsection}{\numberline {4.5.6}Android}{51}{subsection.4.5.6}
\contentsline {subsection}{\numberline {4.5.7}Java}{52}{subsection.4.5.7}
\contentsline {subsubsection}{\numberline {4.5.7.1}Hist\IeC {\'o}ria}{53}{subsubsection.4.5.7.1}
\contentsline {subsubsection}{\numberline {4.5.7.2}Caracter\IeC {\'\i }sticas da linguagem}{53}{subsubsection.4.5.7.2}
\contentsline {subsection}{\numberline {4.5.8}Protocolo HTTP}{53}{subsection.4.5.8}
\contentsline {subsubsection}{\numberline {4.5.8.1}Caracter\IeC {\'\i }sticas}{54}{subsubsection.4.5.8.1}
\contentsline {subsection}{\numberline {4.5.9}NodeJs}{54}{subsection.4.5.9}
\contentsline {subsubsection}{\numberline {4.5.9.1}Hist\IeC {\'o}ria}{54}{subsubsection.4.5.9.1}
\contentsline {subsubsection}{\numberline {4.5.9.2}Caracter\IeC {\'\i }sticas}{55}{subsubsection.4.5.9.2}
\contentsline {subsection}{\numberline {4.5.10}JavaScript}{55}{subsection.4.5.10}
\contentsline {subsubsection}{\numberline {4.5.10.1}Hist\IeC {\'o}ria}{55}{subsubsection.4.5.10.1}
\contentsline {subsubsection}{\numberline {4.5.10.2}Caracter\IeC {\'\i }sticas da linguagem}{56}{subsubsection.4.5.10.2}
\contentsline {subsection}{\numberline {4.5.11}MongoDB}{56}{subsection.4.5.11}
\contentsline {subsubsection}{\numberline {4.5.11.1}Caracter\IeC {\'\i }sticas}{56}{subsubsection.4.5.11.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {M\IeC {\'e}todos}}{58}{chapter.5}
\contentsline {section}{\numberline {5.1}Organiza\IeC {\c c}\IeC {\~a}o das Tarefas}{58}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Trello}{58}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Programas e IDEs}{59}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1} Raspberry Pi }{59}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2} Aplicativo Android }{60}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3} NodeJs }{61}{subsection.5.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Resultados e An\IeC {\'a}lise}}{63}{chapter.6}
\contentsline {section}{\numberline {6.1}Raspberry Pi}{63}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Coleta de Dados OBD}{63}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Teste Aceler\IeC {\^o}metro}{65}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Teste Conex\IeC {\~a}o RFCOMM Bluetooth}{66}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Aplicativo Android: Como Dirijo}{68}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Interface de Usu\IeC {\'a}rio}{68}{subsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.1.1}Tela principal}{68}{subsubsection.6.2.1.1}
\contentsline {subsubsection}{\numberline {6.2.1.2}Visualiza\IeC {\c c}\IeC {\~a}o de infra\IeC {\c c}\IeC {\~o}es em tempo real}{69}{subsubsection.6.2.1.2}
\contentsline {subsubsection}{\numberline {6.2.1.3}Visualiza\IeC {\c c}\IeC {\~a}o do hist\IeC {\'o}rico}{71}{subsubsection.6.2.1.3}
\contentsline {subsubsection}{\numberline {6.2.1.4}Visualiza\IeC {\c c}\IeC {\~a}o da pontua\IeC {\c c}\IeC {\~a}o e detalhes da rota}{72}{subsubsection.6.2.1.4}
\contentsline {subsection}{\numberline {6.2.2}Conex\IeC {\~a}o com Raspberry Pi}{73}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Conex\IeC {\~a}o com servidor}{74}{subsection.6.2.3}
\contentsline {section}{\numberline {6.3}Servidor NodeJs}{75}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Acesso}{75}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Pontua\IeC {\c c}\IeC {\~a}o}{78}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}Banco de dados MongoDB}{80}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Cole\IeC {\c c}\IeC {\~o}es}{80}{subsection.6.4.1}
\contentsline {section}{\numberline {6.5}Integra\IeC {\c c}\IeC {\~a}o}{82}{section.6.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {Conclus\IeC {\~a}o}}{83}{chapter.7}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{85}{section*.12}
