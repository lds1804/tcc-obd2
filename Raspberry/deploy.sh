#!/bin/sh

# this script deploy source files to the raspberry pi
# tip: to avoid entering your password everytime, think about using a generated
# ssh key (see: ssh-keygen and ssh-copy-id)

# deployment start
echo "Deploy to Raspberry Pi: START\n"

RASP_DIR="~/Desktop/TCCRepositorio/raspberry"

# create directory if it not exists in the raspberry pi
ssh pi@raspberrypi.local "mkdir -p $RASP_DIR"
# ignored files must be included in the .rsyncignore file
rsync -av --exclude-from '.rsyncignore' . pi@raspberrypi.local:$RASP_DIR

# deployment end
echo "\nDeploy to Raspberry Pi: END"

