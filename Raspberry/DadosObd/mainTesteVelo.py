from VelocidadeRpm import * 
from aceleracao import *
import os
from bluetooth import *
from time import *
from metodosBluetooth import *
import json 


import Queue 

#reinicia adaptador bluetooth  
os.system("sudo  /etc/init.d/bluetooth  restart")

# adquire velocidade por meio de uma thread


filaAceleracao = Queue.Queue()
filaVelocidade = Queue.Queue()

obdThread= ObdThread(filaVelocidade)
accThread=AceleracaoThread(filaAceleracao)

obdThread.start()
accThread.start()

while True:
	
	client_sock,server_sock=conectaBluetooth()

	try: 
		while True:
				# verifica se tem novos dados oriundos do obd 
				if (filaVelocidade.empty()==False):
					dados=filaVelocidade.get()
					print "Velocidade " + str(dados)
					dados_json = json.dumps({"velocidade" : dados[0], "rpm" : dados[1], "agora" : str(dados[2]) } )
					client_sock.send(str(dados_json))
				
				#verifica se tem novos eventos oriundos da aceleracao 
				if (filaAceleracao.empty()==False):
					dados=filaAceleracao.get() 
					dados_json = json.dumps({"agora" : str(dados[0]) , "aceleracaoX" : dados[1], "aceleracaoY" : dados[2] } )
					print "aceleracao " + str(dados)  
					client_sock.send(str(dados_json))
				

	except IOError:
		client_sock.close()
		server_sock.close()
		os.system("sudo  /etc/init.d/bluetooth  restart")
		print "Erro conexao"
		print "disconnected"
		print "adaptador reiniciado"


	

		