from serialMetodos import  *
import threading
from  serialMetodos import *
import datetime 

def obtemVelocidade(ser) :
	escreveSerial('010d1\r\n',ser) #Requisita a velocidade ao OBD2
	dados_Velocidade=leSerial(ser)
	velocidade=dados_Velocidade[6:8]
	return velocidade


def obtemRpm(ser) :
	escreveSerial('010c1\r\n',ser)  #Requisita  a RPM ao OBD2
	dados_rpm=leSerial(ser)
	rpm1=dados_rpm[6:8]
	rpm2=dados_rpm[9:11]	
	rpm=rpm1+rpm2
	return rpm 

def converteVelocidade(velocidade) :
	try :
            velocidadeInt= int(velocidade,16)
            print velocidadeInt    
        except :
                print "valor de velocidade invalido "
                velocidadeInt=-1
	return velocidadeInt  

def converteRpm(rpm):
	try :
                rpmInt= int(rpm,16)/4
                print rpmInt
        except:
                print "Valor de RPM invalido "
                rpmInt=-1
	return rpmInt


class ObdThread (threading.Thread):
	def __init__(self,result_q):
		threading.Thread.__init__(self)
		self.result_q= result_q 
        
	def run(self):
        #configuracoes iniciais 
        
        #configura serial 
		ser= configuraSerial()
        
        #inicia o relogio de tempo 
		startTime=time.time()
		
		while True:
			
			#obtem o tempo atual 
			agora=datetime.datetime.now()

			#funcao obtem velocidade
			velocidade=obtemVelocidade(ser)

			#converte velocidade de hex para inteiro  
			velocidadeInt=converteVelocidade(velocidade)

			#obtem rpm 
			rpm = obtemRpm(ser)

			#tenta converter rpm 
			rpmInt=converteRpm(rpm)
			
			self.result_q.put([velocidadeInt,rpmInt,agora])

				
    









             
