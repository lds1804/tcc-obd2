import time
import serial
import datetime
import adxl345 
from  serialMetodos import * 
from  velocidadeRpm import *
import json 
from  aceleracao import * 
#@profile

def obtemDados():	
	#Configura serial 
	ser= configuraSerial()

	#Inicia o  relogio de tempo 
	startTime=time.time()

	#Inicia acelerometro 
	accel = adxl345.ADXL345()

	agora=datetime.datetime.now()
	
	#funcao obtem velocidade
	velocidade=obtemVelocidade(ser)
		
	#funcao converte velocidade 
		
	velocidadeInt=converteVelocidade(velocidade)

	#obtem rpm 
	rpm = obtemRpm(ser)
	    
	#tenta converter rpm 
	rpmInt=converteRpm(rpm)
	    	
	                
		
	#Captura os dados de aceleracao 
	

	axes = accel.getAxes(True)
	accX= axes['x']
	accY= axes['y']    
		
		 
	#TODO# Transforma em JSON os dados     
	         
	dados_json = json.dumps({"agora": str(agora) ,"velocidade" : velocidadeInt , "rpm" : rpmInt , "aceleracaoX": accX , "aceleracaoY" : accY} )
	 

	return dados_json


