import serial   
import time 

def  escreveSerial (dado,ser):
	ser.write(dado)
	time.sleep(0.10)
	
def leSerial (ser):
	ser.readline()
	ser.readline()
	return ser.readline()

def configuraSerial () :
	ser = serial.Serial(port='/dev/ttyAMA0',
                baudrate = 9600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=1
           )
	print "Serial configurado"
	return ser
    	
