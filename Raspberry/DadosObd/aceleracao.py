
import adxl345 
import time 
import datetime
import threading


class AceleracaoThread (threading.Thread):
    def __init__(self,result_q):
        threading.Thread.__init__(self)
        self.result_q=result_q

    def run(self):
        #configuracoes iniciais 
        startTime=time.time()
        

        #inicia o acelerometro 
        accel = adxl345.ADXL345()

        vetorOffsets=calculaOffset()

        xOffset=vetorOffsets['offsetX']
        yOffset=vetorOffsets['offsetY']
        zOffset=vetorOffsets['offsetZ']
        
        while True:
			#obtem o tempo atual 
			agora=datetime.datetime.now()
        	
        	#obtem aceleracoes medidas em G
			axes = accel.getAxes(True)
        	
        	#captura as aceleracoes
			accX= axes['x']
			accY= axes['y']
			accZ= axes['z']	

			accXCorrigida=accX-xOffset
 			accYCorrigida=accY-yOffset
 			accZCorrigida=accZ-zOffset

 			vetorEventos=detectaEvento(accXCorrigida,accYCorrigida)
			
			if(vetorEventos[0]==True or vetorEventos[1]==True):
				self.result_q.put([agora,accXCorrigida,accYCorrigida])

			#detecta a mudanca nos valores de offset 
			if( (abs(accXCorrigida)>=0.07) or (abs(accYCorrigida)>=0.06)):
				mudancaOffset=detectaMudancaOffsetXY(xOffset,yOffset)

				if (mudancaOffset[0]==True):
					xOffset=mudancaOffset[2]
					print "Novo valor de xoffset:" + str(xOffset)

				if(mudancaOffset[1]==True):
					yOffset=mudancaOffset[3]
					print "novo valor de Yoffest:" + str(yOffset)		

			


# Metodos para filtrar o offset inicial e a aceleracao da gravidade calculando uma 

#media de 10 amostras 



def calculaOffset() :

	accel = adxl345.ADXL345()

	accX=0
	accY=0
	accZ=0

	for x in range(0,20):

		axes = accel.getAxes(True)

		accX= accX+axes['x']
		accY= accY+axes['y']
		accZ= accZ+axes['z']

		print x 

	mediaX=accX/20
	mediaY=accY/20
	mediaZ=accZ/20

	return { "offsetX": mediaX , "offsetY" : mediaY, "offsetZ" : mediaZ	}



#Detecta se o carro acelerou mais q o limite estabelecido  

def detectaEvento(aceleracaoX,aceleracaoY):

	aceleracaoLateralLimiarY=0.7
	aceleracaoLongitudionalLimiarX=0.6

	if (abs(aceleracaoX)>=aceleracaoLongitudionalLimiarX) :
		eventoX=True

	else :
		eventoX=False 	

	if 	(abs(aceleracaoY)>=aceleracaoLateralLimiarY):
		 eventoY=True

	else :	 
		eventoY= False	 		

	return [eventoX,eventoY]

	


#funcao disparada a cada 5 segundos para detectar variacoes no eixo z e arrumar offset da aceleracao

def detectaMudancaOffsetXY(offsetX,offsetY):

	limiarX= 0.06
	limiarY= 0.07
	accX=0
	accY=0
	accel = adxl345.ADXL345()
	for x in range(0,50):

		axes = accel.getAxes(True)
		accX= accX+axes['x']
		accY= accY+axes['y']
	 
	mediaX=accX/50
	mediaY=accY/50
	
	if (abs(mediaX-offsetX)>=limiarX):
		mudouX=True
		print "Mudou X"
	else : 
		mudouX=False


	if (abs(mediaY-offsetY)>=limiarY):
		mudouY=True
		print "Mudou Y"
	else : 
		mudouY=False	
	 
	return [mudouX,mudouY,mediaX,mediaY] 


 	

			    

	

	


# 	detectaEventoGlobal=detectaEvento(accXCorrigida,accYCorrigida)		
	
# 	if((detectaEventoGlobal[0]==True) or (detectaEventoGlobal[1]==True)):
# 		print "Evento Detectado" 
# 		print detectaEventoGlobal[0] 
# 		print detectaEventoGlobal[1]

	
# 	time.sleep(3)
